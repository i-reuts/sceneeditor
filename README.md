# Scene Editor

This is the main repository of the _Scene_ _Editor_ project for COMP 371 course at Concordia University.

Current task list:

1) Physx - Collision and Picking (plug in PhysX, connect Physx and Entities, synchronize transormations, special shading, pick queries,
for overlapping shapes and selected entities)
Estimate: 2 days --> Illya, Anthony

2) Scene grid and axes display
Estimate: 1 day --> Jonathan

3) Primitives (Cube, Sphere, Plane) .. just create the primitive models (Opt. keyboard shortcuts to import primitives)
Estimate: 1-2 hours --> Illya

4) Rendering pipeline (shading system (lights, shadows, reflections), materials (advanced texturing, metallic, refraction index))
Estimate: 3-4 days (1 day approx)

5) Pick assets and see what we can display for presentation

6) Save scene to be restored later --> Jonathan

7) Attach tool (attach models to each other in a way that transforming the parent propagates to children)
Estimate: 1 day --> Fred

8) Simple UI (precise transformation, color selection, material properties, primitive creation)
Estimate: 1-2 days --> Cedric (research, prototype)